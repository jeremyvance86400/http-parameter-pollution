'use strict';

// requirements
const express = require('express');
const payment = require('./payment');

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });


app.get('/', (req, res) => {
    // We only allow transfer
    if(!req.query || !req.query.action || !req.query.amount) {
        // console.log('Undefined query parameters')
        res.status(400).send('You can only transfer an amount');
        return;
    }
    var validated = validate(req.query.action, req.query.amount);
    validated !== false ? 
        res.send(payment(...validated)) : 
        res.status(400).send('You can only transfer an amount');
});

var validate = (action, amount) => {
    if (typeof action !== 'string' || typeof amount !== 'string') {
        return false;
    }

    var normalisedAction = action.normalize('NFC');

    // Validate normalised action
    if (normalisedAction.length > 10 ||
        normalisedAction.includes('?') || 
        normalisedAction.includes('&') ||
        normalisedAction.includes('%') ||
        normalisedAction.includes('withdraw')) {
        return false;
    }

    var normalisedAmount = Number(amount); // casting to number
    
    // Validate normalised amount
    if (!Number.isInteger(normalisedAmount) ||
        normalisedAmount < 0 ||
        normalisedAmount >= Math.pow(2, 32)/2 - 1) {
            return false;
    }

    return [normalisedAction, normalisedAmount];
}

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = app;
