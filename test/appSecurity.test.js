const request = require('supertest');
const app = require('./app');


describe('security', () => {

    it('Request to transfer and withdraw $100, should return 400 with error message', async () => {
        const res = await request(app)
            .get('/')
            .query({action: 'transfer%3Faction=withdraw', amount: '100'})
        expect(res.statusCode).toEqual(400);
        expect(res.text).toContain('You can only transfer an amount');
    });

    it('Request to transfer a negative amount, should return 400 with error message', async () => {
        const res = await request(app)
            .get('/')
            .query({action: 'transfer', amount: '-100'})
        expect(res.statusCode).toEqual(400);
        expect(res.text).toContain('You can only transfer an amount');
    });

    it('Request to transfer an out of bounds amount, should return 400 with error message', async () => {
        const res = await request(app)
            .get('/')
            .query({action: 'transfer', amount: '2147483648'})
        expect(res.statusCode).toEqual(400);
        expect(res.text).toContain('You can only transfer an amount');
    });

    it('Request to transfer a non-numerical amount should fail', async () => {
        const res = await request(app)
            .get('/')
            .query({action: 'transfer', amount: 'transfer'})
        expect(res.statusCode).toEqual(400);
        expect(res.text).toContain('You can only transfer an amount');
    });

});
